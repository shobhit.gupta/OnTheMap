# iOS App: On The Map

## Status: Front end complete

## About:
A portfolio app to share location and a link with fellow Udacians.

- Adaptive layout. Any orientation at any size.
- Works on any device running iOS 10

## How to build
- Open the .xcworkspace file (not .xcodeproj file)

### For simulator
- Build

### For device
- Open the project settings by clicking on the Project in the Project Navigator on the left side.
- Select the "OnTheMap" target.
- In General (tab) -> Identity (section) change the bundle identifier to your own bundle identifier.
- Build
