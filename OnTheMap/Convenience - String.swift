//
//  Convenience - String.swift
//  OnTheMap
//
//  Created by Shobhit Gupta on 15/12/16.
//  Copyright © 2016 Shobhit Gupta. All rights reserved.
//

import Foundation

extension String {
    
    func length() -> Int {
        return self.characters.count
    }
    
}
