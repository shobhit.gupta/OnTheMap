//
//  Constants.swift
//  OnTheMap
//
//  Created by Shobhit Gupta on 07/12/16.
//  Copyright © 2016 Shobhit Gupta. All rights reserved.
//

import Foundation

struct ViewConstants {
    
    struct StackView {
        struct Separator {
            static let thickness: CGFloat = 1.0
            static let color = UIColor.init(netHex: 0xDBE2E8)!
        }
    }
    
    struct Assets {
        
        struct Icons {
            static let refresh = ""
        }
    }
    
}
